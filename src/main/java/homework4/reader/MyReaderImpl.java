package homework4.reader;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class MyReaderImpl implements MyReader {

    public String read(String filename) throws IOException {
        File file = new File(filename);
        return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    }

    private List<String> getList(String fileName) throws IOException {
        String[] content = read(fileName).split("\n");
        List<String> strings = new ArrayList<>();
        for (String str : content) {
            strings.add(str);
        }
        return strings;
    }

    public List<String> getMaleNames() {
        try {
            return getList(MALE_NAMES);
        } catch (IOException e) {
            System.out.println("Cant read Male names " + e);
            return new ArrayList<String>();
        }
    }
    public List<String> getMaleLastNames() {
        try {
            return getList(MALE_LAST_NAMES);
        } catch (IOException e) {
            System.out.println("Cant read Male lastnames " + e);
            return new ArrayList<String>();
        }
    }
    public List<String> getFemaleNames() {
        try {
            return getList(FEMALE_NAMES);
        } catch (IOException e) {
            System.out.println("Cant read Feale names " + e);
            return new ArrayList<String>();
        }
    }
    public List<String> getFemaleLastNames() {
        try {
            return getList(FEMALE_LAST_NAMES);
        } catch (IOException e) {
            System.out.println("Cant read Female lastnames " + e);
            return new ArrayList<String>();
        }
    }

}
