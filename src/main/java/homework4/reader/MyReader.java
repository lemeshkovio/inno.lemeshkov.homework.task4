package homework4.reader;

import java.io.IOException;
import java.util.List;

public interface MyReader {

    static final String MALE_NAMES = "maleNames.txt";
    static final String MALE_LAST_NAMES = "maleLastNames.txt";
    static final String FEMALE_NAMES = "femaleNames.txt";
    static final String FEMALE_LAST_NAMES = "femaleLastNames.txt";

    String read(String filename) throws IOException;
    List<String> getMaleNames();
    List<String> getMaleLastNames();
    List<String> getFemaleNames();
    List<String> getFemaleLastNames();
}
