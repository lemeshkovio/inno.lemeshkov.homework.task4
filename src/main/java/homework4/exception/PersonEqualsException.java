package homework4.exception;

public class PersonEqualsException extends Exception{

    /**
     * 
     */
    private static final long serialVersionUID = -6025779926983003874L;

    @Override
    public String getMessage() {
        return "Ident persons detected " + super.getMessage();
    }
}
