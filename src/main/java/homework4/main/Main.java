package homework4.main;

import java.util.Collections;
import java.util.List;

import homework4.entity.Person;
import homework4.exception.PersonEqualsException;
import homework4.generator.Generator;
import homework4.sorter.SortInsertImpl;
import homework4.sorter.Sorter;
import homework4.sorter.SorterBubleImpl;

public class Main {
    private static long startTime;
    private static long endTime;

    public static void main(String[] args) {
        Sorter bubbler = new SorterBubleImpl();
        Generator generator = new Generator();

        List<Person> peopls = generator.generate(1000);
        Collections.shuffle(peopls);
        try {
            startTime = getTime();
            bubbler.SortByGender(peopls);
            endTime = getTime();
        } catch (PersonEqualsException e) {
            System.err.println(e);
        }
        double duration = (endTime - startTime)/100_000_000.0;
        
        peopls.forEach(System.out::println);
        System.out.println("Time elapsed " + duration + " sec.");
    }
    private static long getTime() {
        return System.nanoTime();
    }

}
