package homework4.entity;

public class Man extends Person {

    public Man(Sex sex, String name, String lastName, Integer age) {
        super(sex, name, lastName, age);
        
    }

    public Sex getgendor() {
        return super.sex;
        
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((sex == null) ? 0 : sex.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Man other = (Man) obj;
        if (sex != other.sex)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Man " + super.toString();
    }
    
    public int compareByGendor(Woman o) {
        return 1;
    }
    
    public int compareByGendor(Man o) {
        return this.compareByAge(o);
    }

    @Override
    public int compareTo(Person o) {
        return 0;
    }


}
