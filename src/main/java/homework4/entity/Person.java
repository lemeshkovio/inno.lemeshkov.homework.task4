package homework4.entity;

public abstract class Person implements Comparable<Person> {
    private String name;
    private String lastName;
    private Integer age;
    protected Sex sex;


    public Person( Sex sex, String name, String lastName, Integer age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + age;
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == null)
            return false;
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        if (age != other.age)
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return String.format("name '%s %s', age = %d", name, lastName, age);
    }

    public int compareByName(Person o){
        int result = name.compareTo(o.name);
        if (result == 0) {
            return lastName.compareTo(o.lastName);
        }
        return result;
    }

    public int compareByAge(Person o){
        return age.compareTo(o.age);
    }
    
    public int compareByGendor(Person o){
        int result =  sex.compareTo(o.sex);
        if (result == 0) {
            result = o.age.compareTo(age);
        }
        if (result == 0) {
            result = name.compareTo(o.name);
        }
        if (result == 0) {
            result = lastName.compareTo(o.lastName);
        }
        return result;
    }

}
