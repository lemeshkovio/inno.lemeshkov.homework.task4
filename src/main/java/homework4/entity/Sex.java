package homework4.entity;

public enum Sex {
    MALE(1), FEMALE(0);

    private final int id;
    Sex(int id) {
        this.id = id;
    }
    public int getValue() {
        return id;
    }
}
