package homework4.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import homework4.entity.Man;
import homework4.entity.Person;
import homework4.entity.Sex;
import homework4.entity.Woman;
import homework4.reader.MyReader;
import homework4.reader.MyReaderImpl;

public class Generator {
    private MyReader reader = new MyReaderImpl();
    private Random rand = new Random();
    private List<String> maleNames;
    private List<String> maleLastNames;
    private List<String> femaleNames;
    private List<String> femaleLastNames;

    public List<Person> generate(int count) {
        List<Person> persons = new ArrayList<>();
        fillLists();
        for (int i = 0; i < count; i++) {
            if (i%2 == 0) {
                persons.add(createMan());
            } else {
                persons.add(createWoman());
            }
        }
        return persons;
    }
    
    private void fillLists() {
        maleNames = reader.getMaleNames();
        maleLastNames = reader.getMaleLastNames();
        femaleNames = reader.getFemaleNames();
        femaleLastNames = reader.getFemaleLastNames();
    }
    
    private Man createMan() {
        String name = maleNames.get(rand.nextInt(maleNames.size()));
        String lastName = maleLastNames.get(rand.nextInt(maleLastNames.size()));
        int age = rand.nextInt(100);
        return new Man(Sex.MALE, name, lastName, age);
    }
    
    private Woman createWoman() {
        String name = femaleNames.get(rand.nextInt(femaleNames.size()));
        String lastName = femaleLastNames.get(rand.nextInt(femaleLastNames.size()));
        int age = rand.nextInt(100);
        return new Woman(Sex.FEMALE, name, lastName, age);
    }

}
