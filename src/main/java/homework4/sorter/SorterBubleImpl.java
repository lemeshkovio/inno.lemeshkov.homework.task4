package homework4.sorter;

import java.util.List;

import homework4.entity.Person;
import homework4.exception.PersonEqualsException;

public class SorterBubleImpl implements Sorter {

    @Override
    public void sortByName(List<Person> list) throws PersonEqualsException {
        Person temp;
        if (list.size() > 1) {
            for (int x = 0; x < list.size(); x++) {
                for (int i = 0; i < list.size() - 1; i++) {
                    if (list.get(i).equals(list.get(i + 1))) {
                        throw new PersonEqualsException();
                    }
                    if (list.get(i).compareByName(list.get(i + 1)) > 0) {
                        temp = list.get(i);
                        list.set(i, list.get(i + 1));
                        list.set(i + 1, temp);
                    }
                }
            }
        }
    }

    @Override
    public void sortByAge(List<Person> list) throws PersonEqualsException {
        Person temp;
        if (list.size() > 1) {
            for (int x = 0; x < list.size(); x++) {
                for (int i = 0; i < list.size() - 1; i++) {
                    if (list.get(i).compareByAge(list.get(i + 1)) < 0) {
                        temp = list.get(i);
                        list.set(i, list.get(i + 1));
                        list.set(i + 1, temp);
                    }
                }
            } 
        }

    }

    @Override
    public void SortByGender(List<Person> list) throws PersonEqualsException {
        Person temp;
        if (list.size() > 1) {
            for (int x = 0; x < list.size(); x++) {
                for (int i = 0; i < list.size() - 1; i++) {
                    if (list.get(i).compareByGendor(list.get(i + 1)) > 0) {
                        temp = list.get(i);
                        list.set(i, list.get(i + 1));
                        list.set(i + 1, temp);
                    }
                }
            } 
        }
    }



}
