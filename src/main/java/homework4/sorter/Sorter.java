package homework4.sorter;

import java.util.List;

import homework4.entity.Person;
import homework4.exception.PersonEqualsException;

public interface Sorter {
    
    /** Sorts input list by name
     * 
     * @param list - unsorted list
     * @throws PersonEqualsException 
     * 
     */
    void sortByName(List<Person> list) throws PersonEqualsException;
    
    /** Sorts input list by age
     * 
     * @param list - unsorted list
     * @throws PersonEqualsException 
     * 
     */
    void sortByAge(List<Person> list) throws PersonEqualsException;
    
    void SortByGender(List<Person> list) throws PersonEqualsException;
}
