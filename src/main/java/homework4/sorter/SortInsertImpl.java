package homework4.sorter;

import java.util.List;

import homework4.entity.Person;
import homework4.exception.PersonEqualsException;

public class SortInsertImpl implements Sorter {

    @Override
    public void sortByName(List<Person> list) {

        int i, j;

        for (i = 1; i < list.size(); i++) {
            Person temp = list.get(i);
            j = i;
            while ((j > 0) && (list.get(j-1).compareByName(temp) > 0)) {
                list.set(j, list.get(j - 1));
                j--;
            }
            list.set(j, temp);
        }
    }

    @Override
    public void sortByAge(List<Person> list) {
        int i, j;

        for (i = 1; i < list.size(); i++) {
            Person temp = list.get(i);
            j = i;
            while ((j > 0) && (list.get(j-1).compareByAge(temp) < 0)) {
                list.set(j, list.get(j - 1));
                j--;
            }
            list.set(j, temp);
        }
    }

    @Override
    public void SortByGender(List<Person> list) throws PersonEqualsException {
        // TODO Auto-generated method stub
        
    }
    
    
}
