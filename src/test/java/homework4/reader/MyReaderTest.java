package homework4.reader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MyReaderTest {
    
    private static final String TEST_FILE = "testName.txt";
    private static final String UNEXIST_FILE = "test.txt";

    private MyReader reader;
    @BeforeEach
    void setUp() throws Exception {
        reader = new MyReaderImpl();
    }

    @Test
    void test_read_ShouldReadFile() {
        String expect = "testName";
        String test = "";
        try {
            test = reader.read(TEST_FILE);
        } catch (IOException e) {
            System.out.println(e);
        }
        assertEquals(expect, test);
    }
    
    @Test
    void test_rad_ShouldThrowFileNotFoundException() {
        assertThrows(FileNotFoundException.class, () -> reader.read(UNEXIST_FILE));
    }
}
